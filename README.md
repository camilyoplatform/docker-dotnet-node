# Docker .NET SDK + Node.js

## Tags

The tags represent dotnet and node versions.

- `3.1-12.x` - dotnet 3.1 and node 12.x
- `3.1-14.x` - dotnet 3.1 and node 14.x

## Build

```
docker build --pull --rm -t camilyodevacr.azurecr.io/dotnet-node:<dotnetVer-nodeVer> .
```

## Push

1. Login to ACR:

```
az login
az acr login --name camilyodevacr
```

2. Push:

```
docker push camilyodevacr.azurecr.io/dotnet-node:<dotnetVer-nodeVer>
```
